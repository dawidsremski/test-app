package pl.com.sremski.testapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.com.sremski.testapp.domain.ExhibitorDao;

@Controller
public class ShowExhibitorsController extends WebMvcConfigurerAdapter {

    @Autowired
    ExhibitorDao exhibitorDao;

    @RequestMapping("/show-exhibitors")
    public String showExhibitors(Model md) {

        md.addAttribute("exhibitors",exhibitorDao.findAll());
        return "showExhibitors";
    }
}
