package pl.com.sremski.testapp.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.com.sremski.testapp.domain.Exhibitor;
import pl.com.sremski.testapp.domain.ExhibitorDTO;
import pl.com.sremski.testapp.domain.ExhibitorDao;

import javax.validation.Valid;

@Controller
public class AddExhibitorController extends WebMvcConfigurerAdapter {

    @Autowired
    private ExhibitorDao exhibitorDao;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/exhibitor-results").setViewName("addExhibitorResults");
    }

    @RequestMapping("/register-exhibitor")
    public String showForm(ExhibitorDTO exhibitorDTO) {
        return "addExhibitorForm";
    }

    @PostMapping("/register-exhibitor")
    public String checkUserData(@Valid ExhibitorDTO exhibitorDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addExhibitorForm";
        }

        Exhibitor exhibitor = new Exhibitor();
        BeanUtils.copyProperties(exhibitorDTO,exhibitor);
        exhibitorDao.save(exhibitor);
        exhibitorDTO.setId(exhibitor.getId());

        return "addExhibitorResults";
    }
}