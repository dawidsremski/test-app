package pl.com.sremski.testapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.com.sremski.testapp.domain.ExhibitorDao;
import pl.com.sremski.testapp.domain.VisitorDao;

@Controller
public class ShowVisitorsController extends WebMvcConfigurerAdapter {

    @Autowired
    VisitorDao visitorDao;
    @Autowired
    ExhibitorDao exhibitorDao;

    @RequestMapping(path = "/show-visitors")
    String showVisitors(Model md, @RequestParam(value = "exhibitorId", required = false) String exhibitorId) {

        md.addAttribute("visitors",visitorDao.findAllByExhibitors(exhibitorDao.findExhibitorById(exhibitorId)));
        if (exhibitorId == null) md.addAttribute("visitors",visitorDao.findAll());
        return "showVisitors";
    }
}
