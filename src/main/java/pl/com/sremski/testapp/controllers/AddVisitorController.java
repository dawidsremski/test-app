package pl.com.sremski.testapp.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.com.sremski.testapp.domain.Visitor;
import pl.com.sremski.testapp.domain.VisitorDTO;
import pl.com.sremski.testapp.domain.VisitorDao;

import javax.validation.Valid;

@Controller
public class AddVisitorController extends WebMvcConfigurerAdapter {

    @Autowired
    private VisitorDao visitorDao;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/visitor-results").setViewName("addVisitorResults");
    }

    @RequestMapping("/register-visitor/{id}")
    public ModelAndView showForm(@PathVariable String id, VisitorDTO visitorDTO) {
        ModelAndView modelAndView = new ModelAndView("addVisitorForm");
        modelAndView.addObject("eventid",id);
        return modelAndView;
    }

    @PostMapping("/register-visitor")
    public String checkUserData(@Valid VisitorDTO visitorDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addVisitorForm";
        }

        Visitor visitor = new Visitor();
        BeanUtils.copyProperties(visitorDTO, visitor);
        visitorDao.save(visitor);
        visitorDTO.setId(visitor.getId());

        return "addVisitorResults";
    }
}