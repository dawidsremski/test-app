package pl.com.sremski.testapp.domain;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import pl.com.sremski.testapp.validators.UniqueEmail;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@MappedSuperclass
abstract class User {

    private String email;

    private String password;
}
