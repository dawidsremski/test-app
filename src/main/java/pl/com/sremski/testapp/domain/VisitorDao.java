package pl.com.sremski.testapp.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisitorDao extends CrudRepository<Visitor, String> {

    List<Visitor> findAllByEmail(String email);
    List<Visitor> findAllByExhibitors(Exhibitor exhibitor);
}
