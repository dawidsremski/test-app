package pl.com.sremski.testapp.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table (name = "exhibitors")
public class Exhibitor extends User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    private String company;

    @ManyToMany(mappedBy = "exhibitors")
    private Set<Visitor> visitors = new HashSet<>();

}
