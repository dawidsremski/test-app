package pl.com.sremski.testapp.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExhibitorDao extends CrudRepository<Exhibitor, String> {

    List<Exhibitor> findAllByEmail(String email);
    Exhibitor findExhibitorById(String id);
}