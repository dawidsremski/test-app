package pl.com.sremski.testapp.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "visitors")
public class Visitor extends User{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    private String name;

    private String surname;

    private Integer age;

    private String address;

    private String city;

    private String postalCode;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "visits",
            joinColumns = {@JoinColumn(name = "visitor_id")},
            inverseJoinColumns = {@JoinColumn(name = "exhibitor_id")}
    )
    Set<Exhibitor> exhibitors = new HashSet<>();
}