package pl.com.sremski.testapp.domain;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import pl.com.sremski.testapp.services.VisitorService;
import pl.com.sremski.testapp.validators.UniqueEmail;

import javax.validation.constraints.*;

@Data
public class VisitorDTO {

    private String id;

    @NotNull(message = "Prosze podać imię!")
    @Size(min = 2, max = 30, message = "Pole musi zawierać od 2 do 30 znaków!")
    private String name;

    @NotNull
    @Size(min = 2, max = 30, message = "Pole musi zawierać od 2 do 30 znaków!")
    private String surname;

    @NotNull(message = "Proszę podać wiek!")
    @Max(value = 100, message = "Proszę podać poprawny wiek!")
    @Min(value = 0, message = "Proszę podać poprawny wiek!")
    private Integer age;

    @NotNull
    @Size(min = 2, max = 100, message = "Pole musi zawierać od 2 do 100 znaków!")
    private String address;

    @NotNull
    @Size(min = 2, max = 30, message = "Pole musi zawierać od 2 do 30 znaków!")
    private String city;

    @NotNull
    @Pattern(regexp = "\\d{2}-\\d{3}", message = "Proszę podać poprawny kod pocztowy!")
    private String postalCode;

    @NotEmpty(message = "Proszę podać adres e-mail!")
    @Email(message = "Proszę podać poprawny adres e-mail!")
    @UniqueEmail(service = VisitorService.class)
    private String email;

    @NotNull
    @Size(min = 6, message = "Hasło powinno składać się przynajmniej z 6 znaków!")
    private String password;
}

