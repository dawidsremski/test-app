package pl.com.sremski.testapp.domain;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import pl.com.sremski.testapp.services.ExhibitorService;
import pl.com.sremski.testapp.validators.UniqueEmail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ExhibitorDTO {

    private String id;

    @NotNull(message = "Prosze podać firmę!")
    @Size(min = 2, max = 30, message = "Nazwa firmy musi zawierać od 2 do 30 znaków!")
    private String company;

    @NotEmpty(message = "Proszę podać adres e-mail!")
    @Email(message = "Proszę podać poprawny adres e-mail!")
    @UniqueEmail(service = ExhibitorService.class)
    private String email;

    @NotNull
    @Size(min = 6, message = "Hasło powinno składać się przynajmniej z 6 znaków!")
    private String password;
}
