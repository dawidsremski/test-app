package pl.com.sremski.testapp.services;

import org.springframework.stereotype.Service;

@Service
public interface UserService {

    boolean isEmailPresent(String email);
}
