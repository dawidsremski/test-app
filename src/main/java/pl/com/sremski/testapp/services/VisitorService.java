package pl.com.sremski.testapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.com.sremski.testapp.domain.Visitor;
import pl.com.sremski.testapp.domain.VisitorDao;

@Service
public class VisitorService implements UserService {

    @Autowired
    VisitorDao visitorDao;

    @Override
    public boolean isEmailPresent(String email) {
        return !visitorDao.findAllByEmail(email).isEmpty();
    }
}