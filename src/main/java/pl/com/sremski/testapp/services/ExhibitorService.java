package pl.com.sremski.testapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.sremski.testapp.domain.ExhibitorDao;
import pl.com.sremski.testapp.domain.VisitorDao;

@Service
public class ExhibitorService implements UserService {

    @Autowired
    ExhibitorDao exhibitorDao;

    @Override
    public boolean isEmailPresent(String email) {
        return !exhibitorDao.findAllByEmail(email).isEmpty();
    }
}