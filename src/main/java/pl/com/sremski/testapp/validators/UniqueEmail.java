package pl.com.sremski.testapp.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueEmailValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)

public @interface UniqueEmail {

    String message() default "Podany adres e-mail jest już zarejestrowany!";

    Class<?>[] groups() default {};

    Class<?> service();

    Class<? extends Payload>[] payload() default {};
}
