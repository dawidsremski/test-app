package pl.com.sremski.testapp.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import pl.com.sremski.testapp.services.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    @Autowired
    private ApplicationContext applicationContext;

    //@Autowired
    //VisitorService visitorService;
    private UserService userService;

    @Override
    public void initialize(UniqueEmail uniqueEmail) {
        userService = (UserService) applicationContext.getBean(uniqueEmail.service());
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {

            return !userService.isEmailPresent(email);
    }
}